namespace MailumFalls.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DefaultConnection : DbContext
    {
        public DefaultConnection()
            : base("name=DefaultConnection")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<FixedRate> FixedRates { get; set; }
        public virtual DbSet<OverrideRate> OverrideRates { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CustomerFname)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CustomerMname)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.CustomerLname)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRate>()
                .Property(e => e.RateType)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRate>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRate>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRate>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRate>()
                .Property(e => e.RateType)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRate>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRate>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRate>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.PhotoName)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.PhotoURL)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<Photo>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.ItemName)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.CapacityType)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Specification)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.OtherCharges)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<Service>()
                .Property(e => e.Other3)
                .IsUnicode(false);
        }
    }
}
