﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace MailumFalls.Models
{
	[Table("mlDatabase.Customer")]
	public partial class Customer
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int CustomerID { get; set; }

		[Required]
		[StringLength(45)]
		public string CustomerFname { get; set; }

		[Required]
		[StringLength(45)]
		public string CustomerMname { get; set; }

		[Required]
		[StringLength(45)]
		public string CustomerLname { get; set; }

		public int Contact { get; set; }

		[Required]
		[StringLength(150)]
		public string Address { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }
	}

	[Table("mlDatabase.Booking")]
	public partial class Booking
	{
		public int BookingID { get; set; }

		public int BookingNo { get; set; }

		public DateTime BookStart { get; set; }

		public DateTime BookEnd { get; set; }

		[Required]
		[StringLength(45)]
		public string Status { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }

		public int Customer_CustomerID { get; set; }

		public int Service_ItemID { get; set; }

		public int Customer_CustomerID1 { get; set; }

		public int Service_ItemID1 { get; set; }
	}
}