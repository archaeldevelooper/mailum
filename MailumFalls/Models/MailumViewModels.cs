﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace MailumFalls.Models
{
	[Table("mlDatabase.Service")]
	public partial class Service
	{
		[Key]
		public int ItemID { get; set; }

		[Required]
		[StringLength(45)]
		public string ItemName { get; set; }

		[Required]
		[StringLength(45)]
		public string CapacityType { get; set; }

		public int Capacity { get; set; }

		[Required]
		[StringLength(45)]
		public string Specification { get; set; }

		[StringLength(45)]
		public string OtherCharges { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }

		public int FixedRate_FixedRateID { get; set; }

		public int OverrideRate_OverrideRateID { get; set; }

		public int Photos_PhotoID { get; set; }
	}

	[Table("mlDatabase.FixedRate")]
	public partial class FixedRate
	{
		public int FixedRateID { get; set; }

		[Required]
		[StringLength(45)]
		public string RateType { get; set; }

		public decimal Rate { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }
	}

	[Table("mlDatabase.OverrideRate")]
	public partial class OverrideRate
	{
		public int OverrideRateID { get; set; }

		[Required]
		[StringLength(45)]
		public string RateType { get; set; }

		public decimal Rate { get; set; }

		public DateTime BookingPeriod { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }
	}

	[Table("mlDatabase.Photos")]
	public partial class Photo
	{
		public int PhotoID { get; set; }

		[Required]
		[StringLength(45)]
		public string PhotoName { get; set; }

		[Required]
		[StringLength(45)]
		public string PhotoURL { get; set; }

		[StringLength(45)]
		public string Caption { get; set; }

		[Required]
		[StringLength(45)]
		public string Status { get; set; }

		[StringLength(45)]
		public string Other1 { get; set; }

		[StringLength(45)]
		public string Other2 { get; set; }

		[StringLength(45)]
		public string Other3 { get; set; }
	}
}