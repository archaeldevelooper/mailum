﻿$(document).ready(function () {
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	var maxLimitDate = new Date(nowDate.getFullYear() + 1, nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	maxLimitDate = moment(new Date(maxLimitDate)).format("MM/DD/YYYY");

	var _start = getParameterByName("cin");
	var _end = getParameterByName("cout");
	var _typ = getParameterByName("type");
	var _cap = getParameterByName("cap");

	//set default entry
	$('#selectservice').val(_typ);
	$('#selectcapacity').val(_cap);
	$('#myCarousel').carousel('next');
	//parameter header
	$('#paramTyp').html(_typ);
	$('#paramIn').html(_start);
	$('#paramOut').html(_end);
	$('#paramCap').html(_cap);

	//Daterange picker =====================================
	$('#txCheckinDate').daterangepicker({
		"autoApply": true,
		"startDate": _start,
		"endDate": _end,
		"minDate": _start,
		"maxDate": maxLimitDate,
	}, function (start, end) {
		$('#txCheckinDate').parent().parent().removeClass('has error');
	});

	$('#txCheckoutDate').daterangepicker({
		"autoApply": true,
		"startDate": _start,
		"endDate": _end,
		"minDate": _start,
		"maxDate": maxLimitDate,
	}, function (start, end) {
		$('#txCheckoutDate').parent().parent().removeClass('has error');
	});

	$('#txCheckinDate').val(moment(new Date(today)).format("DD MMM YYYY"));
	$('#txCheckinDate').attr('data-Datestart', moment(new Date(today)).format("MM/DD/YYYY"));
	$('#txCheckoutDate').val(moment(new Date(today)).format("DD MMM YYYY"));
	$('#txCheckoutDate').attr('data-Dateend', moment(new Date(today)).format("MM/DD/YYYY"));

	$('#txCheckinDate').on('apply.daterangepicker', function (ev, picker) {
		_start = picker.startDate.format('MM/DD/YYYY');
		_end = picker.endDate.format('MM/DD/YYYY');

		if (picker.startDate.format('YYYY/MM/DD') > moment(new Date(_end)).format("YYYY/MM/DD")) {
			//selected Checkin is greater than Checkout
			$('#txCheckinDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckinDate').data('daterangepicker').setEndDate(_start);
			$('#txCheckinDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtPickup').val(picker.startDate.format('DD/MM/YYYY'));

			$('#txCheckoutDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckoutDate').data('daterangepicker').setEndDate(_start);
			$('#txCheckoutDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtDropOff').val(picker.startDate.format('DD/MM/YYYY'));
			//$('#txCheckoutDate').trigger('click');
		} else {
			$('#txCheckinDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckinDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckinDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtPickup').val(picker.startDate.format('DD/MM/YYYY'));

			$('#txCheckoutDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckoutDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckoutDate').val(moment(new Date(_end)).format("DD MMM YYYY"));
			//$('#txtDropOff').val(moment(new Date(_end)).format("DD/MM/YYYY"));
		};
		$('#txCheckinDate').attr('data-Datestart', _start);
		$('#txCheckoutDate').attr('data-Dateend', _end);
	});
	$('#txCheckinDate').on('cancel.daterangepicker', function (ev, picker) {
		var _startDate = $(this).attr('data-Datestart');
		$('#txCheckinDate').data('daterangepicker').setStartDate(_startDate);
		$('#txCheckinDate').data('daterangepicker').setEndDate(_startDate);
		$('#txCheckinDate').val(moment(new Date($(this).attr('data-Datestart'))).format("DD MMM YYYY"));
	});
	$('#txCheckinDate').on('hide.daterangepicker', function (ev, picker) {
		$('#txCheckinDate').val(moment(new Date($(this).attr('data-Datestart'))).format("DD MMM YYYY"));
	});

	$('#txCheckoutDate').on('apply.daterangepicker', function (ev, picker) {
		_start = picker.startDate.format('MM/DD/YYYY');
		_end = picker.endDate.format('MM/DD/YYYY');

		if (picker.startDate.format('YYYY/MM/DD') < moment(new Date(_start)).format("YYYY/MM/DD")) {
			//selected Checkin is greater than Checkout
			$('#txCheckinDate').data('daterangepicker').setStartDate(_end);
			$('#txCheckinDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckinDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtPickup').val(picker.startDate.format('DD/MM/YYYY'));

			$('#txCheckoutDate').data('daterangepicker').setStartDate(_end);
			$('#txCheckoutDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckoutDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtDropOff').val(moment(new Date(_end)).format("DD MMM YYYY"));
			//$('#txCheckoutDate').trigger('click');
		} else {
			$('#txCheckinDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckinDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckinDate').val(moment(new Date(_start)).format("DD MMM YYYY"));
			//$('#txtPickup').val(picker.startDate.format('DD/MM/YYYY'));

			$('#txCheckoutDate').data('daterangepicker').setStartDate(_start);
			$('#txCheckoutDate').data('daterangepicker').setEndDate(_end);
			$('#txCheckoutDate').val(moment(new Date(_end)).format("DD MMM YYYY"));
			//$('#txtDropOff').val(moment(new Date(_end)).format("DD/MM/YYYY"));
		};
		$('#txCheckinDate').attr('data-Datestart', _start);
		$('#txCheckoutDate').attr('data-Dateend', _end);
	});
	$('#txCheckoutDate').on('cancel.daterangepicker', function (ev, picker) {
		var _endDate = $(this).attr('data-Dateend');
		$('#txCheckoutDate').data('daterangepicker').setStartDate(_endDate);
		$('#txCheckoutDate').data('daterangepicker').setEndDate(_endDate);
		$('#txCheckoutDate').val(moment(new Date($(this).attr('data-Dateend'))).format("DD MMM YYYY"));
	});
	$('#txCheckoutDate').on('hide.daterangepicker', function (ev, picker) {
		$('#txCheckoutDate').val(moment(new Date($(this).attr('data-Dateend'))).format("DD MMM YYYY"));
	});

	//Slider event =========================================
	$('#myCarousel').on('slid.bs.carousel', function (ev) {
		if ($(ev.relatedTarget).hasClass('servicetyp')) {
			$('#slideprev').hide();
		}
		if ($(ev.relatedTarget).hasClass('servicequote')) {
			$('#slideprev').show();
		}
	});

	$('#selectservice').on('change', function () {
		$('#myCarousel').carousel('next');
	});
	$('#slideprev').on('click', function () {
		$('#selectservice').val('0').change();
	});

	$('#btncheckavailability').on('click', function () {
		var servicetyp = $('#selectservice').val();
		var cindate = moment(new Date($('#txCheckinDate').val())).format("MM/DD/YYYY");
		var coutdate = moment(new Date($('#txCheckoutDate').val())).format("MM/DD/YYYY");
		var cap = $('#selectcapacity').val();
		var entryerror = "";

		if (servicetyp == "0") {
			entryerror += "Service type is invalid" + "\n";
		}
		if (moment(new Date($('#txCheckinDate').val())).format("MM/DD/YYYY") > moment(new Date($('#txCheckoutDate').val())).format("YYYY/MM/DD")) {
			//Check-In should be later than or equal to the check-out date.
			entryerror += "Check-In should be later than or equal to the check-out date.";
		}
		if (cap == "0") {
			entryerror += "Capacity type is invalid";
		}
		if (entryerror != "") {
			alert(entryerror)
		} else {
			//Search
			window.location.href = "/Search?type=" + servicetyp + "&cap=" + cap + "&cin=" + cindate + "&cout=" + coutdate;
		}
	})

	getresult();
	function getresult() {
		$.ajax({
			url: '/Search/Getitem?type=' + _typ + "&cap=" + _cap + "&cin=" + _start + "&cout=" + _end,
			contentType: 'application/x-www-form-urlencoded; charset:utf-8',
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				var resultitems = $('#resultbox');
				resultitems.empty();

				if (result.length > 0) {
					$.each(result, function (key, value) {
						resultitems.append(
							'<div class="col-sm-12 mar-top-20" style="background-color: #ddd;padding:10px;">' +
								'<div class="col-sm-3">' +
									'<a href="/Service/Details/' + value.ItemID + '"><img src="' + value.PhotoURL + '" class="img-responsive" />' +
								'</div>' +
								'<div class="col-sm-6">' +
									'<h4>' + value.ItemName + '</h4>' +
									'<i style="color:#449d44">' + value.Rate + '</i>' +
									'<div class="row mar-top-50">' +
										'<div class="col-sm-6">' +
											'<a href="/Service/Rate/' + value.ItemID + '">Inclusives</a>' +
										'</div>' +
										'<div class="col-sm-6">' +
											'<a href="/Service/Details/' + value.ItemID + '">Gallery</a>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="col-sm-3" style="height: 150px;text-align: center;height:150px;">' +
									'<div style="position: relative;top: 50%;transform: translateY(-50%);">' +
										'<a id="btnbooknow" data-item="' + value.ItemID + '" class="btn btn-booknow"> Book Now!</a>' +
									'</div>' +
								'</div>' +
							'</div>'
						);
					});
				} else {
					// resultitems.append(
					// 	'<div class="col-sm-12 mar-top-20" style="background-color: #ddd;padding:10px;">' +
					// 		'<p>No Result Found!</p>' +
					// 	'</div>'
					// );

					resultitems.append(
						'<div class="col-sm-12 mar-top-20" style="background-color: #ddd;padding:10px;">' +
							'<div class="col-sm-3">' +
								'<a href="/Service/Details/?"><img src="/Content/assets/img/loft.jpg" class="img-responsive" />' +
							'</div>' +
							'<div class="col-sm-6">' +
								'<h4>' + _typ + '1</h4>' +
								'<i style="color:#449d44">1000.00</i>' +
								'<div class="row mar-top-50">' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Rate/?">Inclusives</a>' +
									'</div>' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Details/?">Gallery</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="col-sm-3" style="height: 150px;text-align: center;height:150px;">' +
								'<div style="position: relative;top: 50%;transform: translateY(-50%);">' +
									'<a id="btnbooknow" data-item="?" class="btn btn-booknow"> Book Now!</a>' +
								'</div>' +
							'</div>' +
						'</div>' + 
						'<div class="col-sm-12 mar-top-20" style="background-color: #ddd;padding:10px;">' +
							'<div class="col-sm-3">' +
								'<a href="/Service/Details/?"><img src="/Content/assets/img/building.jpg" class="img-responsive" />' +
							'</div>' +
							'<div class="col-sm-6">' +
								'<h4>' + _typ + '2</h4>' +
								'<i style="color:#449d44">1000.00</i>' +
								'<div class="row mar-top-50">' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Rate/?">Inclusives</a>' +
									'</div>' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Details/?">Gallery</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="col-sm-3" style="height: 150px;text-align: center;height:150px;">' +
								'<div style="position: relative;top: 50%;transform: translateY(-50%);">' +
									'<a id="btnbooknow" data-item="?" class="btn btn-booknow"> Book Now!</a>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-sm-12 mar-top-20" style="background-color: #ddd;padding:10px;">' +
							'<div class="col-sm-3">' +
								'<a href="/Service/Details/?"><img src="/Content/assets/img/desk.jpg" class="img-responsive" />' +
							'</div>' +
							'<div class="col-sm-6">' +
								'<h4>' + _typ + '3</h4>' +
								'<i style="color:#449d44">1000.00</i>' +
								'<div class="row mar-top-50">' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Rate/?">Inclusives</a>' +
									'</div>' +
									'<div class="col-sm-6">' +
										'<a href="/Service/Details/?">Gallery</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
							'<div class="col-sm-3" style="height: 150px;text-align: center;height:150px;">' +
								'<div style="position: relative;top: 50%;transform: translateY(-50%);">' +
									'<a id="btnbooknow" data-item="?" class="btn btn-booknow"> Book Now!</a>' +
								'</div>' +
							'</div>' +
						'</div>'
					);
				}

				$('#resultloader').hide();
				$('#resultparam').show();
				$('#resultbox').show();
			}
		});
	}
})
