﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MailumFalls.Startup))]
namespace MailumFalls
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
