﻿using MailumFalls.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MailumFalls.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

		public JsonResult Getitem(string type, string cap, string cin, string cout)
		{
			Repository repo = new Repository();
			List<Service> Userorder = new List<Service>();
			string myemail = User.Identity.Name;
			Userorder = repo.GetItems(type, cap, cin, cout);

			var _result = Json(Userorder, JsonRequestBehavior.AllowGet);
			_result.MaxJsonLength = int.MaxValue;
			return _result;
		}
	}
}