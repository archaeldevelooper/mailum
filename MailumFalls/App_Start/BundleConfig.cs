﻿using System.Web;
using System.Web.Optimization;

namespace MailumFalls
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
					   "~/Scrips/jquery-2.2.0.min.js",
					  "~/Scrips/bootstrap.js"
					  ));

           
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
  

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
					   "~/Content/assets/css/styles.min.css"
					 ));
        }
    }
}
