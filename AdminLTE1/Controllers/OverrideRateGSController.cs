﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class OverrideRateGSController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: OverrideRateGS
        public ActionResult Index()
        {
            return View(db.OverrideRateGS.ToList());
        }

        // GET: OverrideRateGS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRateGS overrideRateGS = db.OverrideRateGS.Find(id);
            if (overrideRateGS == null)
            {
                return HttpNotFound();
            }
            return View(overrideRateGS);
        }

        // GET: OverrideRateGS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OverrideRateGS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OverrideRateID,RateType,Rate,BookingPeriod,Other1,Other2,Other3")] OverrideRateGS overrideRateGS)
        {
            if (ModelState.IsValid)
            {
                db.OverrideRateGS.Add(overrideRateGS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(overrideRateGS);
        }

        // GET: OverrideRateGS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRateGS overrideRateGS = db.OverrideRateGS.Find(id);
            if (overrideRateGS == null)
            {
                return HttpNotFound();
            }
            return View(overrideRateGS);
        }

        // POST: OverrideRateGS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OverrideRateID,RateType,Rate,BookingPeriod,Other1,Other2,Other3")] OverrideRateGS overrideRateGS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(overrideRateGS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(overrideRateGS);
        }

        // GET: OverrideRateGS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRateGS overrideRateGS = db.OverrideRateGS.Find(id);
            if (overrideRateGS == null)
            {
                return HttpNotFound();
            }
            return View(overrideRateGS);
        }

        // POST: OverrideRateGS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OverrideRateGS overrideRateGS = db.OverrideRateGS.Find(id);
            db.OverrideRateGS.Remove(overrideRateGS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
