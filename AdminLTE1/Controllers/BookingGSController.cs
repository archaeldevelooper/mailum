﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class BookingGSController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: BookingGS
        public ActionResult Index()
        {
            return View(db.BookingGS.ToList());
        }

        // GET: BookingGS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingGS bookingGS = db.BookingGS.Find(id);
            if (bookingGS == null)
            {
                return HttpNotFound();
            }
            return View(bookingGS);
        }

        // GET: BookingGS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookingGS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookingID,BookingNo,BookStart,BookEnd,Status,Other1,Other2,Other3,Customer_CustomerID,Service_ItemID,Customer_CustomerID1,Service_ItemID1")] BookingGS bookingGS)
        {
            if (ModelState.IsValid)
            {
                db.BookingGS.Add(bookingGS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bookingGS);
        }

        // GET: BookingGS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingGS bookingGS = db.BookingGS.Find(id);
            if (bookingGS == null)
            {
                return HttpNotFound();
            }
            return View(bookingGS);
        }

        // POST: BookingGS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BookingID,BookingNo,BookStart,BookEnd,Status,Other1,Other2,Other3,Customer_CustomerID,Service_ItemID,Customer_CustomerID1,Service_ItemID1")] BookingGS bookingGS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookingGS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bookingGS);
        }

        // GET: BookingGS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookingGS bookingGS = db.BookingGS.Find(id);
            if (bookingGS == null)
            {
                return HttpNotFound();
            }
            return View(bookingGS);
        }

        // POST: BookingGS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookingGS bookingGS = db.BookingGS.Find(id);
            db.BookingGS.Remove(bookingGS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
