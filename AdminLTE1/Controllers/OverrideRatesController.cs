﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class OverrideRatesController : Controller
    {
        private MFMYSQL db = new MFMYSQL();

        // GET: OverrideRates
        public ActionResult Index()
        {
            return View(db.OverrideRates.ToList());
        }

        // GET: OverrideRates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRate overrideRate = db.OverrideRates.Find(id);
            if (overrideRate == null)
            {
                return HttpNotFound();
            }
            return View(overrideRate);
        }

        // GET: OverrideRates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OverrideRates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OverrideRateID,RateType,Rate,BookingPeriod,Other1,Other2,Other3")] OverrideRate overrideRate)
        {
            if (ModelState.IsValid)
            {
                db.OverrideRates.Add(overrideRate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(overrideRate);
        }

        // GET: OverrideRates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRate overrideRate = db.OverrideRates.Find(id);
            if (overrideRate == null)
            {
                return HttpNotFound();
            }
            return View(overrideRate);
        }

        // POST: OverrideRates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OverrideRateID,RateType,Rate,BookingPeriod,Other1,Other2,Other3")] OverrideRate overrideRate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(overrideRate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(overrideRate);
        }

        // GET: OverrideRates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OverrideRate overrideRate = db.OverrideRates.Find(id);
            if (overrideRate == null)
            {
                return HttpNotFound();
            }
            return View(overrideRate);
        }

        // POST: OverrideRates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OverrideRate overrideRate = db.OverrideRates.Find(id);
            db.OverrideRates.Remove(overrideRate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
