﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class ServiceGSController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: ServiceGS
        public ActionResult Index()
        {
            return View(db.ServiceGS.ToList());
        }

        // GET: ServiceGS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceGS serviceGS = db.ServiceGS.Find(id);
            if (serviceGS == null)
            {
                return HttpNotFound();
            }
            return View(serviceGS);
        }

        // GET: ServiceGS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ServiceGS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ItemID,ItemName,CapacityType,Capacity,Specification,OtherCharges,Other1,Other2,Other3,FixedRate_FixedRateID,OverrideRate_OverrideRateID,Photos_PhotoID")] ServiceGS serviceGS)
        {
            if (ModelState.IsValid)
            {
                db.ServiceGS.Add(serviceGS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(serviceGS);
        }

        // GET: ServiceGS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceGS serviceGS = db.ServiceGS.Find(id);
            if (serviceGS == null)
            {
                return HttpNotFound();
            }
            return View(serviceGS);
        }

        // POST: ServiceGS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ItemID,ItemName,CapacityType,Capacity,Specification,OtherCharges,Other1,Other2,Other3,FixedRate_FixedRateID,OverrideRate_OverrideRateID,Photos_PhotoID")] ServiceGS serviceGS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceGS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(serviceGS);
        }

        // GET: ServiceGS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceGS serviceGS = db.ServiceGS.Find(id);
            if (serviceGS == null)
            {
                return HttpNotFound();
            }
            return View(serviceGS);
        }

        // POST: ServiceGS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceGS serviceGS = db.ServiceGS.Find(id);
            db.ServiceGS.Remove(serviceGS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
