﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class FixedRateGSController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: FixedRateGS
        public ActionResult Index()
        {
            return View(db.FixedRateGS.ToList());
        }

        // GET: FixedRateGS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRateGS fixedRateGS = db.FixedRateGS.Find(id);
            if (fixedRateGS == null)
            {
                return HttpNotFound();
            }
            return View(fixedRateGS);
        }

        // GET: FixedRateGS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FixedRateGS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FixedRateID,RateType,Rate,Other1,Other2,Other3")] FixedRateGS fixedRateGS)
        {
            if (ModelState.IsValid)
            {
                db.FixedRateGS.Add(fixedRateGS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fixedRateGS);
        }

        // GET: FixedRateGS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRateGS fixedRateGS = db.FixedRateGS.Find(id);
            if (fixedRateGS == null)
            {
                return HttpNotFound();
            }
            return View(fixedRateGS);
        }

        // POST: FixedRateGS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FixedRateID,RateType,Rate,Other1,Other2,Other3")] FixedRateGS fixedRateGS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fixedRateGS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fixedRateGS);
        }

        // GET: FixedRateGS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FixedRateGS fixedRateGS = db.FixedRateGS.Find(id);
            if (fixedRateGS == null)
            {
                return HttpNotFound();
            }
            return View(fixedRateGS);
        }

        // POST: FixedRateGS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FixedRateGS fixedRateGS = db.FixedRateGS.Find(id);
            db.FixedRateGS.Remove(fixedRateGS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
