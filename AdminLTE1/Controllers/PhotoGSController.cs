﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MailumFalls.Models;

namespace AdminLTE1.Controllers
{
    public class PhotoGSController : Controller
    {
        private GSMYSQL db = new GSMYSQL();

        // GET: PhotoGS
        public ActionResult Index()
        {
            return View(db.PhotoGS.ToList());
        }

        // GET: PhotoGS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhotoGS photoGS = db.PhotoGS.Find(id);
            if (photoGS == null)
            {
                return HttpNotFound();
            }
            return View(photoGS);
        }

        // GET: PhotoGS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PhotoGS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PhotoID,PhotoName,PhotoURL,Caption,Status,Other1,Other2,Other3")] PhotoGS photoGS)
        {
            if (ModelState.IsValid)
            {
                db.PhotoGS.Add(photoGS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(photoGS);
        }

        // GET: PhotoGS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhotoGS photoGS = db.PhotoGS.Find(id);
            if (photoGS == null)
            {
                return HttpNotFound();
            }
            return View(photoGS);
        }

        // POST: PhotoGS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PhotoID,PhotoName,PhotoURL,Caption,Status,Other1,Other2,Other3")] PhotoGS photoGS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(photoGS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(photoGS);
        }

        // GET: PhotoGS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PhotoGS photoGS = db.PhotoGS.Find(id);
            if (photoGS == null)
            {
                return HttpNotFound();
            }
            return View(photoGS);
        }

        // POST: PhotoGS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PhotoGS photoGS = db.PhotoGS.Find(id);
            db.PhotoGS.Remove(photoGS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
