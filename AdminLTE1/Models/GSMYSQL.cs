namespace MailumFalls.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class GSMYSQL : DbContext
    {
        public GSMYSQL()
            : base("name=GSMYSQL")
        {
        }

        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<FixedRate> FixedRates { get; set; }
        public virtual DbSet<OverrideRate> OverrideRates { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }
        public virtual DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookingGS>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<BookingGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<BookingGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<BookingGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.CustomerFname)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.CustomerMname)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.CustomerLname)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRateGS>()
                .Property(e => e.RateType)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRateGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRateGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<FixedRateGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRateGS>()
                .Property(e => e.RateType)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRateGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRateGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<OverrideRateGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.PhotoName)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.PhotoURL)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<PhotoGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.ItemName)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.CapacityType)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.Specification)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.OtherCharges)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.Other1)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.Other2)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceGS>()
                .Property(e => e.Other3)
                .IsUnicode(false);
        }

        public System.Data.Entity.DbSet<MailumFalls.Models.BookingGS> BookingGS { get; set; }

        public System.Data.Entity.DbSet<MailumFalls.Models.FixedRateGS> FixedRateGS { get; set; }

        public System.Data.Entity.DbSet<MailumFalls.Models.OverrideRateGS> OverrideRateGS { get; set; }

        public System.Data.Entity.DbSet<MailumFalls.Models.PhotoGS> PhotoGS { get; set; }

        public System.Data.Entity.DbSet<MailumFalls.Models.ServiceGS> ServiceGS { get; set; }
    }
}
