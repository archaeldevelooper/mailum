namespace MailumFalls.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("mlDatabase.Photos")]
    public partial class Photo
    {
        public int PhotoID { get; set; }

        [Required]
        [StringLength(45)]
        public string PhotoName { get; set; }

        [Required]
        [StringLength(45)]
        public string PhotoURL { get; set; }

        [StringLength(45)]
        public string Caption { get; set; }

        [Required]
        [StringLength(45)]
        public string Status { get; set; }

        [StringLength(45)]
        public string Other1 { get; set; }

        [StringLength(45)]
        public string Other2 { get; set; }

        [StringLength(45)]
        public string Other3 { get; set; }
    }
}
