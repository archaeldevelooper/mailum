namespace MailumFalls.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gsDatabase.OverrideRate")]
    public partial class OverrideRateGS
    {
        [Key]
        public int OverrideRateID { get; set; }

        [Required]
        [StringLength(45)]
        public string RateType { get; set; }

        public decimal Rate { get; set; }

        public DateTime BookingPeriod { get; set; }

        [StringLength(45)]
        public string Other1 { get; set; }

        [StringLength(45)]
        public string Other2 { get; set; }

        [StringLength(45)]
        public string Other3 { get; set; }
    }
}
